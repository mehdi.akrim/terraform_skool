<!--- 
Ceci est la version en markdown !
Utilisez l'aperçu pour avoir une version plus lisible
-->
# Terraform - TP1 : CLI - Utilisation basique

> **Objectifs du TP** :
>- Valider l’installation de Terraform
>- Valider l’accès au compte AWS
>- Se familiariser avec les commandes de base de Terraform
>
> **Prérequis :**
>- Une machine avec un navigateur
>- Un accès à internet
>
> **Niveau de difficulté** : Débutant

## 1- Objectifs détaillés

Pour les besoins de l’exercice, nous avons pré-instancié un VPC et un subnet public.

Nous allons commencer par prendre en main l'outil en ligne de commande permettant de créer et gérer des ressources AWS. 

L’objectif sera de déployer :
* une instance EC2
  * dans un VPC (pré-instancié)
  * dans un subnet public (pré-instancié)
  * utilisant le security group par défaut


## 2- Création de la Topology

Regardons le fichier topology.tf

Vérifier/corriger la syntaxe terraform:
```bash
$ terraform fmt
```

Initialiser le provider:
```bash
$ terraform init
```

Visualiser le plan d'exécution:
```bash
$ terraform plan
```

Appliquer le plan d'exécution:
```bash
$ terraform apply
```

Les ressources sont maintenant créées.

## 3- Un petit coup de ménage

Pour détruire votre topology :
```bash
$ terraform destroy
```

## 4- Bonus: Voir le graphe de dépendance

Pour consulter le graphe votre topology :
```bash
$ terraform graph
```