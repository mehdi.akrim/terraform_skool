# Création d'une instance dans le security group créé précédement
resource "aws_instance" "my_instance" {
  ami           = "ami-0d73480446600f555" # Ubuntu AMI
  instance_type = "t2.micro"              # Taille de l'instance https://www.ec2instances.info/
  subnet_id     = "subnet-00ad7dbc7bba17897"

  tags = {
    Name      = "instance-TF-TP1-MEAK"
    Formation = "terraform"
    User      = "MEAK"
  }
}
