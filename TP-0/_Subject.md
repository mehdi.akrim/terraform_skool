<!--- 
Ceci est la version en markdown !
Utilisez l'aperçu pour avoir une version plus lisible
-->
# Terraform - TP0 : Installation Terraform

> **Objectifs du TP** :
>- Installer l’outillage nécessaire pour accéder à AWS
>- Installer Terraform
>
> **Prérequis :**
>- Une machine avec un navigateur
>- Un accès à internet
>
> **Niveau de difficulté** : Débutant

## 1- Installer Terraform

Téléchargez le binaire [terraform](https://www.terraform.io/downloads.html) dans le repertoire courant

```sh
$ wget https://releases.hashicorp.com/terraform/0.14.4/terraform_0.14.4_linux_amd64.zip
$ sudo unzip terraform_0.14.4_linux_amd64.zip -d /usr/local/bin/
$ rm terraform_0.14.4_linux_amd64.zip

```

Il n’y a pas de besoin d’access key ni de secret key
L’authentification avec les apis d’AWS se fait via le mécanisme d’instance profile


## 2- Valider l'installation de Terraform

Vérifier que la version de terraform correspond à la version téléchargée.

```sh
$ terraform --version
Terraform v0.14.4
```