variable "trigramme" {
  type = string
  default = "MEAK"
}

variable "tp" {
  type = string
  default = "TP4"
}

variable "subnet_cidr_block" {
  type = string
  default = "10.0.12.0/24"
}

variable "availability_zone_suffix" {
  type = string
  default = "a"
}

variable "Formation" {
  type = string
}
