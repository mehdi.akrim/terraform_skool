
resource "aws_subnet" "my_subnet" {
  availability_zone = local.availability_zone
  cidr_block        = var.subnet_cidr_block
  vpc_id            = data.aws_vpc.my_vpc.id
  tags = {
    Name      = "subnet-TF-${local.resource_name_suffix}"
    Formation = "terraform"
    User      = var.trigramme
  }
}

resource "aws_security_group" "my_security_group" {
  vpc_id      = data.aws_vpc.my_vpc.id
  name_prefix = "TF-${var.tp}_SG"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # add a CIDR block here
  }

  tags = {
    Name      = "sg-TF-${local.resource_name_suffix}"
    Formation = "terraform"
    User      = var.trigramme
  }
}

resource "aws_instance" "jenkins" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.medium"
  subnet_id              = aws_subnet.my_subnet.id
  vpc_security_group_ids = [ aws_security_group.my_security_group.id ]

  tags = {
    Name      = "jenkins-TF-${local.resource_name_suffix}"
    Formation = "terraform"
    User      = var.trigramme
  }
}

resource "aws_instance" "jenkins-slave" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.micro"
  subnet_id              = aws_subnet.my_subnet.id
  vpc_security_group_ids = [ aws_security_group.my_security_group.id ]

  tags = {
    Name      = "jenkins-slave-TF-${local.resource_name_suffix}"
    Formation = "terraform"
    User      = var.trigramme
  }
}