<!--- 
Ceci est la version en markdown !
Utilisez l'aperçu pour avoir une version plus lisible
-->
# Terraform - TP2: Syntaxe HCL - Utilisation basique

> **Objectifs du TP** :
>- Se familiariser avec la déclaration de ressources AWS via Terraform
>- Manipuler les commandes de base de Terraform
>- Comprendre le fonctionnement et la manipulation des ressources
>
> **Prérequis :**
>- Une machine avec un navigateur
>- Un accès à internet
>
> **Niveau de difficulté** : Débutant

## 1- Objectifs détaillés

Dans ce TP, nous allons déployer une Topologie composée de :
* un subnet
* un Security Group autorisant le port 80 en entrée
* une instance
* un disque attaché à l’instance

![Architecture](../medias/TP2-archi.png)

Nous allons donc utiliser différentes ressources aws.

La documentation terraform associé à AWS est disponible [ici](https://registry.terraform.io/providers/hashicorp/aws/latest/docs)

Attention, la documentation commence par la partie datasource, dans ce tp nous utiliserons que la partie resource disponible en dessous.


## 2- Création de la topology

Ouvrir le fichier topology.tf et y implementer les ressources décrites ci-dessous avec les informations suivantes:

Un subnet, s'appelant my_subnet, composé:
* d'un attribut "availability_zone" avec la valeur: "us-east-1a"
* d'un attribut "cidr_block" avec la valeur: "10.0.12.0/24"
* d'un attribut "vpc_id" avec la valeur: "vpc-0b4048156c0de9005"
* d’un tag "Name" valant "subnet-TF-TP2-MEAK"
* d’un tag "Formation" valant "terraform"
* d’un tag "User" valant "MEAK"

Un security group, s'appelant my_security_group, composé:
* d'un attribut "name_prefix"  valant "TF-TP2_SG"
* d'un attribut "vpc_id" avec la valeur: "vpc-0b4048156c0de9005"
* d’un tag "Name" valant "sg-TF-TP2-MEAK"
* d’un tag "Formation" valant "terraform"
* d’un tag "User" valant "MEAK"

Une règle de security group, s'appelant my_security_group_rule_out_http, composée:
* d'un attribut "type" correspondant au flux sortant
* d'un attribut "from_port" valant "80"
* d'un attribut "to_port" valant "80"
* d'un attribut "protocol" valant "tcp"
* d'un attribut "cidr_blocks" acceptant toutes les IPs
* d'un attribut "security_group_id" correspondant à l’id du security group créé précédemment

Une règle de security group, s'appelant my_security_group_rule_out_https, composée:
* d'un attribut "type" correspondant au flux sortant
* d'un attribut "from_port" valant "443"
* d'un attribut "to_port" valant "443"
* d'un attribut "protocol" valant "tcp"
* d'un attribut "cidr_blocks" acceptant toutes les IPs
* d'un attribut "security_group_id" correspondant à l’id du security group créé précédemment

Une règle de security group, s'appelant my_security_group_rule_http_in, composée:
* d'un attribut "type" correspondant au flux entrant
* d'un attribut "from_port" correspondant au port 80
* d'un attribut "to_port" correspondant au port 80
* d'un attribut "protocol"  correspondant à tcp
* d'un attribut "cidr_blocks" acceptant toutes les IPs
* d'un attribut "security_group_id" correspondant à l’id du security group créé précédemment

Une instance, s'appelant my_instance, composée:
* d'un attribut "ami" avec la valeur: "ami-0d73480446600f555"
* d'un attribut "subnet_id" correspondant à l’id du subnet créé précédemment
* d'un attribut "instance_type" correspondant à une VM de  type T3 disposant de 2 vCPU et 4Go RAM (cf: http://www.ec2instances.info/)
* d'un attribut "vpc_security_group_ids" correspondant à l’id du security group créé précédemment
* d’un tag "Name" valant "instance-TF-TP2-MEAK"
* d’un tag "Formation" valant "terraform"
* d’un tag "User" valant "MEAK"

Vérifier/corriger la syntaxe terraform:
```bash
$ terraform fmt
```

Initialiser le provider:
```bash
$ terraform init
```

Visualiser le plan d'exécution:
```bash
$ terraform plan
```

Appliquer le plan d'exécution:
```bash
$ terraform apply
```

Les ressources sont maintenant créées.

## 3- Un petit coup de ménage

Pour détruire votre topology :
```bash
$ terraform destroy
```
