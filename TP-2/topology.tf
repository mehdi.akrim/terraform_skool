resource "aws_subnet" "my_subnet" {
  vpc_id     = "vpc-0b4048156c0de9005"
  cidr_block = "10.0.12.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name = "subnet-TF-TP2-MEAK"
    Formation = "terraform"
    User = "MEAK"
  }
}

// security group
resource "aws_security_group" "my_security_group" {
  name = "TF-TP2_SG"
  vpc_id = "vpc-0b4048156c0de9005"
  tags = {
    Name = "sg-TF-TP2-MEAK"
    Formation = "terraform"
    User = "MEAK"
  }
}

// security group rule 1
resource "aws_security_group_rule" "my_security_group_rule_out_http" {
  type              = "egress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.my_security_group.id
}

// security group rule 2
resource "aws_security_group_rule" "my_security_group_rule_out_https" {
  type              = "egress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.my_security_group.id
}

// security group rule 3
resource "aws_security_group_rule" "my_security_group_rule_http_in" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.my_security_group.id
}

// instance
resource "aws_instance" "my_instance" {
  ami           = "ami-0d73480446600f555"
  instance_type = "t3a.medium"
  subnet_id = aws_subnet.my_subnet.id
  vpc_security_group_ids = [aws_security_group.my_security_group.id]
  tags = {
    Name = "instance-TF-TP2-MEAK"
    Formation = "terraform"
    User = "MEAK"
  }
}