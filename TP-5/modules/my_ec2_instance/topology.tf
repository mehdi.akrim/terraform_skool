resource "aws_security_group" "my_security_group" {
  vpc_id      = var.vpc_id
  name_prefix = "TF-${var.tp}-SG"
  tags = {
    Name      = "sg-TF-${local.name_suffix}"
    Formation = "terraform"
    User      = upper(var.trigramme)
  }
}

resource "aws_instance" "my_instance" {
  ami                    = "ami-0d73480446600f555"
  instance_type          = var.instance_type
  vpc_security_group_ids = [aws_security_group.my_security_group.id]
  subnet_id              = var.subnet_id

  tags = {
    Name      = "instance-TF-${local.name_suffix}-${var.app}"
    App       = var.app
    Formation = "terraform"
    User      = upper(var.trigramme)
  }
}
