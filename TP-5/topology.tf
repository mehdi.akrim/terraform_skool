resource "aws_subnet" "my_subnet" {
  availability_zone = "${data.aws_region.current.name}a"
  cidr_block        = "10.0.12.0/24"
  vpc_id            = data.aws_vpc.my_vpc.id
  tags = {
    Name      = "subnet-TF-${var.tp}-${var.trigramme}"
    Formation = "terraform"
    User      = var.trigramme
  }
}
