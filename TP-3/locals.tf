locals {
    availability_zone = "${data.aws_region.current.name}${var.availability_zone_suffix}"
    resource_name_suffix = "${var.tp}-${var.trigramme}"
}