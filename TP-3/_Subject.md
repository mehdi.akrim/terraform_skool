<!--- 
Ceci est la version en markdown !
Utilisez l'aperçu pour avoir une version plus lisible
-->
# Terraform - TP3: Syntaxe HCL - Rendre son code paramétrable

> **Objectifs du TP** :
>- Se familiariser avec la paramétisation du code Terraform
>
> **Prérequis :**
>- Une machine avec un navigateur
>- Un accès à internet
>
> **Niveau de difficulté** : Intermédiaire

## 1- Objectifs détaillés

Dans le TP 2, nous avons créé une topology dont les valeurs étaient codées en dur.

Nous allons maintenant dans le TP 3:
* rendre notre code paramétrable avec les variables, les datasources et les locals
* surcharger des valeurs de variables
* sortir des informations en console via la commande output

## 2- Mise à jour de la topology

Editer les fichiers suivants en y implementant les ressources décrites ci-dessous

Dans le fichier **variables.tf** on doit retrouver les variables :
* trigramme
* tp
* subnet_cidr_block
* availability_zone_suffix

Dans le fichier **terraform.tfvars** qui doit avoir les champs :
* trigramme valant "MEAK"
* tp valant "TF-TP3"
* subnet_cidr_block valant "10.0.12.0/24"
* availability_zone_suffix valant "a"

Dans le fichier **datasources.tf** qui doit avoir :
* un datasource, s'appelant my_vpc, qui doit chercher un vpc filtré par:
  * le tag "Env" et par la valeur "TP"
  * le tag "User" et par la valeur "SIDE"
* un datasource, s'appelant `current`, qui doit chercher la région courante (sans filtre ou paramètre, comportement par défaut)

Dans le fichier **locals.tf** qui doit avoir les champs :
* "availability_zone" valant "${data.aws_region.current.name}${var.availability_zone_suffix}"
* "resource_name_suffix" valant "${var.tp}-${var.trigramme}"

Dans le fichier **output.tf** qui doit avoir les champs :
* "instance_ip" valant "${aws_instance.my_instance.private_ip}"

Remplacer dans le fichier **topology.tf** les valeurs suivantes par l’appels aux variables/datasource :
* [local] availability_zone (aws_subnet)    
* [variable] cidr_block (aws_subnet)
* [datasource] vpc_id (aws_subnet, aws_security_group)
* [variable] tp (aws_security_group)
* [local] resource_name_suffix (aws_subnet, aws_security_group, aws_instance)

Vérifier/corriger la syntaxe terraform:
```bash
$ terraform fmt
```

Initialiser le provider:
```bash
$ terraform init
```

Visualiser le plan d'exécution:
```bash
$ terraform plan
```

Appliquer le plan d'exécution:
```bash
$ terraform apply
```

Vous devriez voir en Output l’ip de l’instance

## 3- Bonus

Essayer d’utiliser au maximum les variables et les datasources pour les champs suivants:

Variables:
* Formation

Datasource:
* AMI, la dernière ubuntu par exemple


## 4- Un petit coup de ménage

Pour détruire votre topology :
```bash
$ terraform destroy
```
