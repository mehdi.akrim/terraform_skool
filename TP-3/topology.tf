resource "aws_subnet" "my_subnet" {
  availability_zone = "${local.availability_zone}"
  cidr_block        = "${var.subnet_cidr_block}"
  vpc_id            =  data.aws_vpc.my_vpc.id
  tags = {
    Name      = "subnet-TF-TP3-MEAK"
    Formation = "terraform"
    User      = "MEAK"
  }
}

resource "aws_security_group" "my_security_group" {
  vpc_id      = data.aws_vpc.my_vpc.id
  name_prefix = "${var.tp}_SG"
  tags = {
    Name      = "sg-${local.resource_name_suffix}"
    Formation = "terraform"
    User      = "MEAK"
  }
}

resource "aws_security_group_rule" "my_security_group_rule_out_http" {
  type              = "egress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.my_security_group.id
}

resource "aws_security_group_rule" "my_security_group_rule_out_https" {
  type              = "egress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.my_security_group.id
}

resource "aws_security_group_rule" "my_security_group_rule_http_in" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.my_security_group.id
}

resource "aws_instance" "my_instance" {
  ami                    = "ami-0d73480446600f555"
  instance_type          = "t2.medium"
  vpc_security_group_ids = [aws_security_group.my_security_group.id]
  subnet_id              = aws_subnet.my_subnet.id

  tags = {
    Name      = "instance-TF-TP3-MEAK"
    Formation = "terraform"
    User      = "MEAK"
  }
}